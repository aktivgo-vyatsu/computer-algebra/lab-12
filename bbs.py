import math


def bbs(p, q, count) -> dict:
    __bbs_validate(p, q)
    m = p * q
    x = __generate_x(m)
    x0 = pow(x, 2) % m
    result = [int(math.pow(x0, 2) % m)]
    for i in range(1, count):
        result.append(int(math.pow(result[i - 1], 2) % m))

    res = {}
    for r in result:
        res[r] = r & 1
    return res


def __bbs_validate(p, q):
    if p % 4 != 3:
        raise ValueError(f'p validation error: {p} % 4 != 3')
    if q % 4 != 3:
        raise ValueError(f'q validation error: {q} % 4 != 3')


def __generate_x(m) -> float:
    x = 2
    while True:
        if math.gcd(m, x) == 1:
            return x
